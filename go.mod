module gitlab.digilol.net/laurynas/kip

go 1.19

require (
	github.com/go-chi/chi/v5 v5.0.8
	github.com/shurcooL/graphql v0.0.0-20220606043923-3cf50f8a0a29
)

require (
	golang.org/x/net v0.4.0
	golang.org/x/text v0.5.0 // indirect
)
