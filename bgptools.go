package main

import (
	"bufio"
	"encoding/csv"
	"encoding/json"
	"io"
	"log"
	"net"
	"os"
	"strconv"
)

var (
	bgptoolsAsnNames map[uint]*string
	bgptoolsTables map[*net.IPNet]*uint
)

func bgptoolsAsnNameInit() {
	bgptoolsAsnNames = make(map[uint]*string)
	f, err := os.Open(c.bgptoolsAsnNameExport)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	r := csv.NewReader(f)
	for {
		rec, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}
		if rec[0] == "asn" {
			continue
		}
		asn, err := strconv.ParseUint(rec[0][2:], 10, 32)
		if err != nil {
			log.Println(err)
			continue
		}
		bgptoolsAsnNames[uint(asn)] = &rec[1]
	}
}

func bgptoolsAsnName(asn uint) (name *string) {
	value, ok := bgptoolsAsnNames[asn]
	if ok {
		name = value
	}
	return
}

func bgptoolsTablesInit() {
	bgptoolsTables = make(map[*net.IPNet]*uint)
	f, err := os.Open(c.bgptoolsTableVisibilityExport)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	r := bufio.NewReader(f)
	d := json.NewDecoder(r)
	type Message struct {
		CIDR string
		ASN, Hits uint
	}
	for d.More() {
		var m Message
		err := d.Decode(&m)
		if err != nil {
			log.Println(err)
			continue
		}
		_, ipNet, err := net.ParseCIDR(m.CIDR)
		if err != nil {
			log.Println(err)
			continue
		}
		bgptoolsTables[ipNet] = &m.ASN
	}
}

func bgptoolsIpAsn(ip string) (asn *uint, prefix *string) {
	searchIP := net.ParseIP(ip)
	var ipNet *net.IPNet
	for ipNet, asn = range bgptoolsTables {
		if ipNet.Contains(searchIP) {
			if p := ipNet.String(); p != "" {
				prefix = &p
			}
			return
		}
	}
	asn = nil
	return
}
