#!/bin/sh
wget https://check.torproject.org/torbulkexitlist -O torbulkexitlist.txt
wget https://bgp.tools/table.jsonl -O table.jsonl
wget https://bgp.tools/asns.csv -O asns.csv
wget https://iptoasn.com/data/ip2country-v4.tsv.gz -O ip2country-v4.tsv.gz
gunzip ip2country-v4.tsv.gz
wget https://iptoasn.com/data/ip2country-v6.tsv.gz -O ip2country-v6.tsv.gz
gunzip ip2country-v6.tsv.gz
