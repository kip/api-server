package main

import (
	"context"
	"unsafe"

	"github.com/shurcooL/graphql"
)

var irrdGraphqlClient *graphql.Client

type asnInfoResp struct {
	Org *string `json:"org"`
	MntBy []string `json:"mnt-by"`
	Notify []string `json:"notify"`
	Prefixes []string `json:"prefixes"`
	Updated string `json:"updated"`
	Source string `json:"updated"`
}

type asnPrefixesResp struct {
	Prefixes []string `json:"prefixes"`
}

func irrdGraphqlConnect() {
	irrdGraphqlClient = graphql.NewClient(c.irrdGraphql, nil)
}

func asnInfo(ctx context.Context, asn uint) (resp asnInfoResp, err error) {
	var q struct {
		RpslObjects []struct {
			MntBy []graphql.String
			Notify []graphql.String
			Updated graphql.String
			Source graphql.String
		} `graphql:"rpslObjects(asn:[$i],objectClass:[\"aut-num\"])"`
		AsnPrefixes []struct {
			Prefixes []graphql.String
		} `graphql:"asnPrefixes(asns:[$i])"`
	}
	type ASN uint
	a := map[string]interface{} {"i": ASN(asn)}
	err = irrdGraphqlClient.Query(ctx, &q, a)
	if err != nil {
		return
	}
	if len(q.RpslObjects) < 1 || len(q.AsnPrefixes) < 1 {
		err = errAsnNotFound
		return
	}
	d := q.RpslObjects[0]
	resp.MntBy = *(*[]string)(unsafe.Pointer(&d.MntBy))
	resp.Notify = *(*[]string)(unsafe.Pointer(&d.Notify))
	resp.Prefixes = *(*[]string)(unsafe.Pointer(&q.AsnPrefixes[0].Prefixes))
	resp.Updated = string(d.Updated)
	resp.Source = string(d.Source)
	resp.Org = bgptoolsAsnName(asn)
	return
}

func asnPrefixes(ctx context.Context, asn uint) (resp asnPrefixesResp, err error) {
	var q struct {
		AsnPrefixes []struct {
			Prefixes []graphql.String
		} `graphql:"asnPrefixes(asns:[$i])"`
	}
	type ASN uint
	a := map[string]interface{} {"i": ASN(asn)}
	err = irrdGraphqlClient.Query(ctx, &q, a)
	if err != nil {
		return
	}
	if len(q.AsnPrefixes) < 1 {
		err = errAsnNotFound
		return
	}
	resp.Prefixes = *(*[]string)(unsafe.Pointer(&q.AsnPrefixes[0].Prefixes))
	return
}
