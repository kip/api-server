package main

import (
	"bufio"
	"log"
	"os"
)

var torExitList []string

func torExitListInit() {
	f, err := os.Open(c.torBulkExitList)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	s := bufio.NewScanner(f)
	s.Split(bufio.ScanLines)
	for s.Scan() {
		torExitList = append(torExitList, s.Text())
	}
}

func torIsExit(ip string) bool {
	for _, v := range torExitList {
		if v == ip {
			return true
		}
	}
	return false
}
