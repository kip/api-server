package main

import "errors"

var errAsnNotFound = errors.New("ASN Not Found.")
var errAsnInvalid = errors.New("Please enter a valid ASN.")
var errIpInvalid = errors.New("Please enter a valid IP address.")
