package main

import (
	"net/http"
	"strconv"
	"encoding/json"
	"net"
	"strings"

	"github.com/go-chi/chi/v5"
)

type errorResponse struct {
	Status int `json:"status"`
	Code *int `json:"code,omitempty"`
	Message string `json:"message"`
}

type ipInfoResp struct {
	Hostname *string `json:"hostname"`
	Asn *uint `json:"asn"`
	Country *string `json:"country"`
	Org *string `json:"org"`
	Route *string `json:"route"`
	Services struct {
		Tor bool `json:"tor"`
	} `json:"services"`
}

func writeError(w http.ResponseWriter, status int, code *int, message string) {
	w.WriteHeader(status)
	json.NewEncoder(w).Encode(errorResponse{
		Status: status,
		Code: code,
		Message: message,
	})
}

func getAsnHandler(w http.ResponseWriter, r *http.Request) {
	asn, err := strconv.ParseUint(chi.URLParam(r, "i"), 10, 32)
	if err != nil {
		writeError(w, http.StatusBadRequest, nil, errAsnInvalid.Error())
		return
	}
	resp, err := asnInfo(r.Context(), uint(asn))
	if err != nil {
		if err == errAsnNotFound {
			writeError(w, http.StatusNotFound, nil, err.Error())
		} else {
			writeError(w, http.StatusInternalServerError, nil, err.Error())
		}
		return
	}
	json.NewEncoder(w).Encode(resp)
}

func getAsnPrefixesHandler(w http.ResponseWriter, r *http.Request) {
	asn, err := strconv.ParseUint(chi.URLParam(r, "i"), 10, 32)
	if err != nil {
		writeError(w, http.StatusBadRequest, nil, err.Error())
		return
	}
	resp, err := asnPrefixes(r.Context(), uint(asn))
	if err != nil {
		if err == errAsnNotFound {
			writeError(w, http.StatusNotFound, nil, err.Error())
		} else {
			writeError(w, http.StatusInternalServerError, nil, err.Error())
		}
		return
	}
	json.NewEncoder(w).Encode(resp)
}

func getIpHandler(w http.ResponseWriter, r *http.Request) {
	ip := chi.URLParam(r, "i")
	if net.ParseIP(ip) == nil {
		writeError(w, http.StatusBadRequest, nil, errIpInvalid.Error())
		return
	}
	resp := ipInfoResp{}
	resp.Asn, resp.Route = bgptoolsIpAsn(ip)
	if resp.Asn == nil {
		writeError(w, http.StatusNotFound, nil, errAsnNotFound.Error())
		return
	}
	resp.Country = ipToCountry(ip)
	resp.Org = bgptoolsAsnName(*resp.Asn)
	if ptr, _ := net.LookupAddr(ip); len(ptr) > 0 {
		h := strings.TrimSuffix(ptr[0], ".")
		resp.Hostname = &h
	}
	if ptr, _ := net.LookupAddr(ip); len(ptr) > 0 {
		h := strings.TrimSuffix(ptr[0], ".")
		resp.Hostname = &h
	}
	resp.Services.Tor = torIsExit(ip)
	json.NewEncoder(w).Encode(resp)
}
