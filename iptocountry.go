package main

import (
	"bytes"
	"encoding/csv"
	"io"
	"log"
	"net"
	"os"
)

type ipRange struct {
	start, end net.IP
}

var ipToCountryMap map[*ipRange]*string

func ipToCountryParseFile(file string) {
	f, err := os.Open(file)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	r := csv.NewReader(f)
	r.Comma = '\t'
	for {
		rec, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}
		if rec[2] == "None" {
			continue
		}
		ipr := ipRange{net.ParseIP(rec[0]), net.ParseIP(rec[1])}
		ipToCountryMap[&ipr] = &rec[2]
	}
}

func ipToCountryInit() {
	ipToCountryMap = make(map[*ipRange]*string)
	ipToCountryParseFile(c.ipv4ToCountryCodeFile)
	ipToCountryParseFile(c.ipv6ToCountryCodeFile)
}

func ipToCountry(ip string) (country *string) {
	searchIP := net.ParseIP(ip)
	var ipr *ipRange
	for ipr, country = range ipToCountryMap {
		if bytes.Compare(searchIP, ipr.start) >= 0 && bytes.Compare(searchIP, ipr.end) <= 0 {
			return
		}
	}
	country = nil
	return
}
