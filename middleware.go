package main

import "net/http"

func middlewareServerHeader(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Server", "ASNoFun/0.0.1")
		next.ServeHTTP(w, r)
	})
}

func middlewareApiHeaders(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("Cache-Control", "public,max-age=" + c.apiCacheMaxAge +",immutable")
		next.ServeHTTP(w, r)
	})
}
