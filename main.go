package main

import (
	"context"
	"flag"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"syscall"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"golang.org/x/net/http2"
	"golang.org/x/net/http2/h2c"
)

type config struct {
	bindAddr string
	irrdGraphql string
	apiCacheMaxAge string
	bgptoolsAsnNameExport string
	bgptoolsTableVisibilityExport string
	torBulkExitList string
	ipv4ToCountryCodeFile string
	ipv6ToCountryCodeFile string
}

var c config

func readConfig() {
	var apiCacheMaxAge time.Duration
	flag.StringVar(&c.bindAddr, "bind", "localhost:8080", "Binding address")
	flag.StringVar(&c.irrdGraphql, "irrd-graphql", "http://localhost:8000/graphql", "IRRd GraphQL endpoint")
	flag.DurationVar(&apiCacheMaxAge, "api-cache-max-age", 12 * time.Hour, "API Cache-Control headers max-age value")
	flag.StringVar(&c.bgptoolsAsnNameExport, "bgptools-asn-name-export", "asns.csv", "bgp.tools ASN Name export file")
	flag.StringVar(&c.bgptoolsTableVisibilityExport, "bgptools-table-visibility-export", "table.jsonl", "bgp.tools Table visibility export file")
	flag.StringVar(&c.torBulkExitList, "tor-bulk-exit-list", "torbulkexitlist.txt", "Tor bulk exit list file")
	flag.StringVar(&c.ipv4ToCountryCodeFile, "ipv4-to-country-code-map", "ip2country-v4.tsv", "IPv4 to country code map")
	flag.StringVar(&c.ipv6ToCountryCodeFile, "ipv6-to-country-code-map", "ip2country-v6.tsv", "IPv6 to country code map")
	flag.Parse()
	c.apiCacheMaxAge = strconv.Itoa(int(apiCacheMaxAge.Seconds()))
}

func initRouter() http.Handler {
	r := chi.NewRouter()
	r.Use(middleware.RequestID)
	r.Use(middleware.RealIP)
	r.Use(middleware.Logger)
	r.Use(middlewareServerHeader)
	r.Route("/api", func(r chi.Router) {
		r.Use(middlewareApiHeaders)
		r.Route("/asn/{i}", func(r chi.Router) {
			r.Get("/", getAsnHandler)
			r.Get("/prefixes", getAsnPrefixesHandler)
		})
		r.Route("/ip/{i}", func(r chi.Router) {
			r.Get("/", getIpHandler)
		})
	})
	return r
}

func runServer() {
	h2s := &http2.Server{}
	srv := &http.Server{
		Addr: c.bindAddr,
		Handler: h2c.NewHandler(initRouter(), h2s),
	}
	serverCtx, serverStopCtx := context.WithCancel(context.Background())
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)
	go func() {
		<-sig
		shutdownCtx, _ := context.WithTimeout(serverCtx, 30 * time.Second)
		go func() {
			<-shutdownCtx.Done()
			if shutdownCtx.Err() == context.DeadlineExceeded {
				log.Fatal("Graceful shutdown timed out... Forcing exit.")
			}
		}()
		err := srv.Shutdown(shutdownCtx)
		if err != nil {
			log.Fatal(err)
		}
		serverStopCtx()
	}()
	err := srv.ListenAndServe()
	if err != nil && err != http.ErrServerClosed {
		log.Fatal(err)
	}
	<-serverCtx.Done()
}

func main() {
	readConfig()
	irrdGraphqlConnect()
	bgptoolsAsnNameInit()
	bgptoolsTablesInit()
	torExitListInit()
	ipToCountryInit()
	runServer()
}
